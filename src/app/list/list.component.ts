import { Component, OnInit } from '@angular/core';
import { ListService } from '../list.service';
import { Item } from '../model/Item';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  data: Item[];
  text: string;
  constructor(
    private listService: ListService,
    private router: Router) { }

  ngOnInit() {
    this.listService.getAll().do(res => this.setData).subscribe(
      () => {

      }
    );
  }

  setData(data) {
    this.text = JSON.stringify(data);
    this.data = data;
  }

}
