import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpRequest } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { HttpEventType } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { User } from './model/User';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/shareReplay';

// import { url } from '../assets/js/define.js';
// import { url } from './define';

import * as moment from 'moment';

@Injectable()
export class AuthService {

  public url: String;

  constructor(private http: HttpClient) {
    this.url = '/';
  }

  login(id: string, password: string) {
    return this.http.post<User>(this.url + 'api/v1/login', { id, password })
      .do(res => this.setSession)
      .shareReplay();  }

  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expiresIn, 'second');

    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    console.log('Logged of');
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }
}
