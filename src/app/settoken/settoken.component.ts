import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settoken',
  templateUrl: './settoken.component.html',
  styleUrls: ['./settoken.component.css']
})
export class SettokenComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  public token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ';

  constructor() { }

  ngOnInit() {
    localStorage.setItem('id_token', this.token);
  }

}
