import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettokenComponent } from './settoken.component';

describe('SettokenComponent', () => {
  let component: SettokenComponent;
  let fixture: ComponentFixture<SettokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
