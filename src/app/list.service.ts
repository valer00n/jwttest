import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpRequest } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { HttpEventType } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { User } from './model/User';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';

// import { url } from '../assets/js/define.js';
// import { url } from './define';

import * as moment from 'moment';
import { Item } from './model/Item';

@Injectable()
export class ListService {

  public url: String;

  constructor(private http: HttpClient) {
    this.url = '/';
  }

  getAll() {
    return this.http.get(this.url + 'api/v1/list', {headers: this.jwt()});
  }

  // private helper methods
  private jwt() {
    // create authorization header with jwt token
    const id_token = localStorage.getItem('id_token');
    if (id_token) {
      const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + id_token });
      return headers;
    }
  }
}
